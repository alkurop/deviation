package com.alkurop.deviation;

import com.github.alkurop.mvpbase.IBaseScreenView;

/**
 * Created by alkurop on 8/29/16.
 */
public interface IMainView extends IBaseScreenView{
    void changeTiltPosition (int sky, int ground);
    void setTiltLabel(int tilt);
    void setRollLabel(int roll);
    void setSkyGroundRoll(int roll);
    void showNotSupported();
}
