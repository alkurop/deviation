package com.alkurop.deviation;

import com.github.alkurop.mvpbase.BaseScreenPresenter;

/**
 * Created by alkurop on 8/29/16.
 */
public class MainPresenter extends BaseScreenPresenter<IMainModel, IMainView> implements IMainPresenter {
    public MainPresenter (IMainModel model, IMainView view){
        super(model, view);
    }

    @Override
    public void onTiltChange (int tilt){
        view.changeTiltPosition(tilt, 180 - tilt);
        view.setTiltLabel(tilt);
    }

    @Override
    public void onRollChange (int roll){
        view.setSkyGroundRoll(roll);
        view.setRollLabel(roll);
    }

    @Override
    public void onGyroscopeNotSupported (){
        view.showNotSupported();
    }
}
