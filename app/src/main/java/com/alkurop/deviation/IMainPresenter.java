package com.alkurop.deviation;

import com.github.alkurop.mvpbase.IBaseScreenPresenter;

/**
 * Created by alkurop on 8/29/16.
 */
public interface IMainPresenter extends IBaseScreenPresenter {
    void onTiltChange(int tilt);
    void onRollChange(int roll);
    void onGyroscopeNotSupported ();
}
