package com.alkurop.deviation;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.alkurop.mvpbase.BaseActivity;
import com.github.alkurop.mvpbase.IBaseScreenPresenter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements IMainView {

    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.groundSkyContainer)
    LinearLayout groundSkyContainer;

    @BindView(R.id.ground)
    View groundView;

    @BindView(R.id.sky)
    View skyView;

    @BindView(R.id.tvTilt)
    TextView textTilt;

    @BindView(R.id.tvRoll)
    TextView textRoll;

    IMainPresenter presenter;
    SensorManager sensorManager;
    Sensor gyroscope;
    SensorEventListener sensorEventListener;

    LinearLayout.LayoutParams skyParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0);
    LinearLayout.LayoutParams groundParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0);

    @Override
    protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initGroundSkyContainer();
        presenter = new MainPresenter(new MainModel(), this);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        if(gyroscope == null || sensorManager == null ) {
            presenter.onGyroscopeNotSupported();
        }
        initSensorEventListener();
    }

    private void initGroundSkyContainer (){
        groundSkyContainer.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw (){
                groundSkyContainer.getViewTreeObserver().removeOnPreDrawListener(this);
                int width = groundSkyContainer.getMeasuredWidth();
                groundSkyContainer.getLayoutParams().height = width;
                return true;
            }
        });
    }

    @NonNull
    private void initSensorEventListener (){
        sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged (SensorEvent event){
                if(event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                    return;
                }
                presenter.onTiltChange((int) Math.abs(event.values[1]));
                presenter.onRollChange((int) (event.values[2]));
            }

            @Override
            public void onAccuracyChanged (Sensor sensor, int accuracy){
            }
        };
    }

    @Override
    protected void onResume (){
        super.onResume();
        sensorManager.registerListener(sensorEventListener, gyroscope, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause (){
        sensorManager.unregisterListener(sensorEventListener);
        super.onPause();
    }

    @NotNull
    @Override
    public View getSnackbarAnchor (){
        return container;
    }

    @Nullable
    @Override
    public IBaseScreenPresenter getPresenter (){
        return presenter;
    }

    @Override
    public void changeTiltPosition (int sky, int ground){
        skyParams.weight = sky;
        groundParams.weight = ground;

        skyView.setLayoutParams(skyParams);
        groundView.setLayoutParams(groundParams);
    }

    @Override
    public void setTiltLabel (int tilt){
        textTilt.setText(String.format(getString(R.string.tilt), tilt));
    }

    @Override
    public void setRollLabel (int roll){
        textRoll.setText(String.format(getString(R.string.roll), roll));
    }

    @Override
    public void setSkyGroundRoll (int roll){
        groundSkyContainer.setRotation(roll);
    }

    @Override
    public void showNotSupported (){
        groundSkyContainer.setVisibility(View.GONE);
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(getString(R.string.not_supported))
                .setMessage(getString(R.string.will_now_close))
                .setPositiveButton( getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick (DialogInterface dialog, int which){
                        finish();
                    }
                })
                .create();

        showDialog(dialog);
    }
}
