package com.alkurop.deviation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by alkurop on 8/29/16.
 */
@RunWith(JUnit4.class)
public class MainPresenterTest {
    @Mock
    IMainView view;
    @Mock
    IMainModel model;
    IMainPresenter presenter;

    @Before
    public void initMockito (){
        MockitoAnnotations.initMocks(this);
        presenter = new MainPresenter(model, view);
    }

    @Test
    public void testOnTiltChange () throws Exception{
        int tiltChange = (int) (Math.random() * 100);

        int tiltChangeSky = tiltChange;
        int tiltChangeGround = 180 - tiltChange;
        presenter.onTiltChange(tiltChange);

        verify(view, times(1)).changeTiltPosition(tiltChangeSky, tiltChangeGround);
        verify(view, times(1)).setTiltLabel(tiltChange);
    }

    @Test
    public void testOnRollChange () throws Exception{
        int rollChange = (int) (Math.random() * 100);


        presenter.onRollChange(rollChange);

        verify(view, times(1)).setSkyGroundRoll(rollChange);
        verify(view, times(1)).setRollLabel(rollChange);
    }

    @Test
    public void testOnGyroscopeNotSupported () throws Exception{
        presenter.onGyroscopeNotSupported();
        verify(view, times(1)).showNotSupported();
    }
}